<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;
use DB;

class TestController extends Controller
{
    public function index()
    {
        $user = UserTest::all();
        return view('index', compact('user'));
    }

    public function store(Request $request)
    {
        $user = new UserTest;
        $user->name = $request->name;
        $userlist = UserTest::orderBy('id','DESC')->first();
        // dd($userlist);
        if($userlist == null)
        {
            $user->parity = 'Odd';
        }elseif ($userlist->id % 2 == 0)
        {
            $user->parity = 'Odd';
        }else
        {
            $user->parity = 'Even';
        }
        $user->save();
        return redirect()->route('index');
    }

}
